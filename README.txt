TODO:

version or branch:

+ Create main fragment layout as per wireframes
+ Create listview element layout
+ setup ids for all elements
+ put text in string resources
+ create db
	+ pre-populated test entries in each table (actually, did the whole production thing)
	+ put a cool hidden entry in the table, something about cracking my app
+ integrate db
	+ contract class /  classes
	+ helper class
		+ use this lib: https://github.com/jgilfelt/android-sqlite-asset-helper
		+ do not forget to make it a Singleton

- design/implement other dialogs/fragment layouts
	- initial dialog
	- add drink fragment
	
		+ wire app add button to show another add drink fragment
		+ add localised resourse: array of drink types
		+ connect drink type array to spinner
			+ basic test
			+ spinner selection updates drink list from db
		- drinks are sorted by use number, then by alphabet (case-insensitive)
		- keyboard is hidden as start
		- keyboard button is changed to "Search"
		+ custom layout for elements (drink name + alco % because same brand may vary)
			+ tap should show correct colours
		- custom layout for Spinner (bigger list): http://www.broculos.net/2013/09/how-to-change-spinner-text-size-color.html#.UwnvUfl_t9k
		- tap on drink functionality
			- new drink edit dialog
				+ basic toast to catch short tap
				+ create custom dialog from DialogFragment
				+ dialog title - will be "New Drink"
				+ drink name - dedicated textview in layout
				+ dialog gets selected drink _id and loads its name
				+ dialog gets selected drink type and saves it
				+ dialog loads drink types and shows in a spinner using drink type from above for filtering
				+ spinner has custom amount to show volume name, volume and units
				- spinner shows best match volumes first, then the rest
				- dialog allows for custom amount
				- dialog created new consumed drink db entry
				- dialog allows consumed db entry editing
			- add to db and only then increment use count (provide settings option to disable/clear select numbers)
			- kill current fragment after adding the drink. User should be returned to the main View

	- long-tap menu
	- settings menu
	- shared prefs activity
		- create all options with separators w/o actual actions

- think about working with db in async mode
- work with db via those bits of gui

--- at this point the app is almost functional ---

- implement BAC calculation in a dedicated function/class
- call it on GUI redraw (in some call)
- implement notification + service(which would constantly calculate BAC)
		- should it stop once the limit reaches zero?
- implement settings
- intall the latest version of holo lib
- design statistics
- implement statistics
- update all jar libs 
- test in strict mode
- google some automatic testers
- make a list of manual tests
- sign the app, get google play account
- start g+ beta-testers group
- test the app
- publish with ads 
...

- all input text fields must use same grayish colour as GUI
- make an easter egg
	for example, taping BAC 12 times gives out some funny message or hidden story about myself
		- this could help with promotion in future. developer who cares, who is old-school


BUGS:

version or branch:

- Style is not copied to v11 API resources
+ artifact stripe between list and header (fixed by adding 1dp top margin for ListView)
- progress bar "time remaining" is not segmented into string resouces. will be a pita during localisation


This project is a Copyright by Oleg Tikhonov. Distribution of this source code is not allowed, if you found it,
please contact the author at oitikhonov@gmail.com.