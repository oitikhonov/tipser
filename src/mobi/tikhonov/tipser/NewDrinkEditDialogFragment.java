package mobi.tikhonov.tipser;

import mobi.tikhonov.tipser.TipserDBContract.DrinkVolumes;
import mobi.tikhonov.tipser.TipserDBContract.Drinks;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class NewDrinkEditDialogFragment extends DialogFragment {
	
	public static final String DRINK_ID = "mobi.tikhonov.tipser.drink_id";
	
	/*
	 * these 3 member variables store selected drink information
	 * which gets loaded from the DB using an _id which is passed
	 * by the calling fragment to this dialog
	 */
	private long drink_id;
	private long drink_type;
	private String drink_name;
	
	private long selected_volume_id;
	
	private boolean custom_volume_flag = false;
	private long custom_volume;
	private long custom_volume_type;
	
	private TextView drink_name_TextView = null;
	private Spinner volume_types_Spinner = null;
	//private TextView volume_units_TextView = null; 	 // will not be used. we change in setViewBinder
	private EditText custom_amount_EditText = null;
	private Spinner custom_amount_volume_types_Spinner = null;
	
	
	public static NewDrinkEditDialogFragment newInstance(long _id) {
		
		NewDrinkEditDialogFragment fragment = new NewDrinkEditDialogFragment();
		
		if(_id >= 0) {
			Bundle args = new Bundle();
			args.putLong(DRINK_ID, _id);
			fragment.setArguments(args);
		}
		
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.tipser_newdrinkedit_dialog_layout, null);
		
		drink_name_TextView = (TextView) dialogView.findViewById(R.id.newdrinkedit_drinkname_textView);
		//volume_units_TextView = (TextView) dialogView.findViewById(R.id.newdrinkedit_spinner_element_volume_label_TextView);
		volume_types_Spinner = (Spinner) dialogView.findViewById(R.id.newdrinkedit_volumetypes_spinner);
		
		custom_amount_EditText = (EditText) dialogView.findViewById(R.id.newdrinkedit_customamount_editText);
		custom_amount_volume_types_Spinner = (Spinner) dialogView.findViewById(R.id.newdrinkedit_customamount_type_spinner);
		
		if (getArguments() != null) {
			drink_id =  getArguments().getLong(DRINK_ID);
			new LoadDrinkCursorTask(drink_id).execute();
		}
		
		//builder.setTitle(R.string.dialog_drink_amount_title).setView(dialogView)
		builder.setView(dialogView) // try without ugly title
		.setPositiveButton(R.string.dialog_add_button, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
			/*
			 * TODO
			 * 	- make async DB insert using member variables we've collected
			 * 	- check variables first to avoid adding empty data
			 *  - return to main fragment
			 *  - check settings and increment use count for selected drink
			 */
				
			}
		})
		.setNegativeButton(R.string.dialog_cancel_button, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// do nothing
				
			}
		})
		.create();
		
		return builder.create();
	}
	
	private class LoadDrinkCursorTask extends AsyncTask<Void, Void, Void> {

		private Cursor local_cursor = null;
		private long drink_id = -1;
		
		LoadDrinkCursorTask(long id) {
			this.drink_id = id;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// don't forget to sort by drink type and then alphabetically,
			// case-insensitive
			local_cursor = TipserDBHelper.getInstance(getActivity().getApplicationContext()).getReadableDatabase().query(
					Drinks.TABLE_NAME + "en", // fix locale later
					new String[] { Drinks._ID, Drinks.COLUMN_DRINK_NAME, Drinks.COLUMN_DRINK_TYPE},
					Drinks._ID + " = " + drink_id, null,
					null, null, null);
			local_cursor.getCount();

			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			
			local_cursor.moveToFirst();
			drink_name = local_cursor.getString(local_cursor.getColumnIndex(Drinks.COLUMN_DRINK_NAME));
			drink_type = local_cursor.getLong(local_cursor.getColumnIndex(Drinks.COLUMN_DRINK_TYPE));
			
			drink_name_TextView.setText(drink_name);
			
			new LoadMatchingVolumesCursorTask(drink_type).execute();
		}
	}
	
	private class LoadMatchingVolumesCursorTask extends AsyncTask<Void, Void, Void> {

		private Cursor local_cursor = null;
		private long drink_type = -1;	// this task gets the type of selected drink
										// it's up to this task to select all the matching
										// volumes and populate the spinner
		LoadMatchingVolumesCursorTask(long type) {
			this.drink_type = type;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// don't forget to sort by COLUMN_USED_FLAG and then alphabetically,
			// case-insensitive
			
			
			local_cursor = TipserDBHelper.getInstance(getActivity().getApplicationContext()).getReadableDatabase().query(
					DrinkVolumes.TABLE_NAME + "en", // fix locale later
					null, // at this point we may need all data about volumes
					null, null, // update later: universal volumes, filtering via
					null, null, null);											// locale from settings to pick needed units, etc
			
			local_cursor.getCount();

			return (null);
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onPostExecute(Void arg0) {
			
/*			local_cursor.moveToFirst();
			drink_name = local_cursor.getString(local_cursor.getColumnIndex(Drinks.COLUMN_DRINK_NAME));
			drink_type = local_cursor.getLong(local_cursor.getColumnIndex(Drinks.COLUMN_DRINK_TYPE));
			
			drink_name_TextView.setText(drink_name);*/

			SimpleCursorAdapter adapter;
			
			// if(SOME_SETTINGS == ML) OR volume type is such and such
			// do the stuff below

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				adapter = new SimpleCursorAdapter(
						getActivity(),
						R.layout.tipser_newdrinkedit_volumespinner_element_layout,
						local_cursor,
						new String[] { 
							    DrinkVolumes.COLUMN_VOLUME_NAME,
							    DrinkVolumes.COLUMN_VOLUME_ML,		// we update it later down the code
							    DrinkVolumes.COLUMN_VOLUME_TYPE}, 
						new int[] {
								R.id.newdrinkedit_spinner_element_drink_name_Spinner,
								R.id.newdrinkedit_spinner_element_volume_textView,
								R.id.newdrinkedit_spinner_element_volume_label_TextView},
						0);
			} else {
				adapter = new SimpleCursorAdapter(
						getActivity(),
						android.R.layout.simple_spinner_item,
						local_cursor,
						new String[] {
							DrinkVolumes.COLUMN_VOLUME_NAME},
						new int[] {
							android.R.id.text1});
			}
			
			adapter.setViewBinder(new ViewBinder() {
				
				@Override
				public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
					
					// set unit labels properly
					if(view.getId() == R.id.newdrinkedit_spinner_element_volume_label_TextView) {
						
						//cursor.moveToFirst(); // no reason as cursor will already be moved as needed
												
						switch(cursor.getInt(cursor.getColumnIndex(DrinkVolumes.COLUMN_VOLUME_TYPE)))
						{
						case 1:
							((TextView) view).setText(R.string.ml_lowercase);
							break;
						case 2:
							((TextView) view).setText(R.string.floz_lowercase);
							break;
						case 3:
							((TextView) view).setText(R.string.floz_lowercase);
							break;
						case 4:
							((TextView) view).setText("");
							break;
						case 5:
							((TextView) view).setText(R.string.ml_lowercase); // fix later, depends on locale
							break;
						case 6:	 // custom amount
							((TextView) view).setText("");
							break;
						default:
							((TextView) view).setText("");
							break;		
						}
												
						return true;
					}
					
					if(view.getId() == R.id.newdrinkedit_spinner_element_volume_textView) {
						
						switch(cursor.getInt(cursor.getColumnIndex(DrinkVolumes.COLUMN_VOLUME_TYPE)))
						{
						case 2:
						case 3:
							/*
							 * some nasty round up glitch. if we get Long which is 1.5 we receive 1.0
							 * maybe, the better way it to get String, then cast it to Long
							 */
							/*long amount_in_oz = cursor.getLong(cursor.getColumnIndex(DrinkVolumes.COLUMN_VOLUME_OZ));
							Log.w("LoadMatchingVolumesCursorTask", "amount in oz = " + amount_in_oz);
							((TextView) view).setText(Long.toString(amount_in_oz));*/
							
							// I think that cursor is the same as local_cursor from this async_task
							// we can access any data because we load everything in local_cursor
							((TextView) view).setText(cursor.getString(cursor.getColumnIndex(DrinkVolumes.COLUMN_VOLUME_OZ)));
							return true;
						case 5:
							return false; // implement later, don't forget to make it true
						case 6:	// custom amount
							((TextView) view).setText("");
							return true;
						}
						
					}
					
					return false;
				}
			});
			
			/*
			 * the code above does this:
			 * 	- show volumes in ML
			 * 	- if some volumes must be shown in oz (as per volume type in DB) update volume and label
			 * 
			 * TODO:
			 * 	- get preferred units from settings
			 *	- if it's ML:
			 *		- do the stuff as above
			 *	- if it's OZ:
			 *		- do the opposite code:
			 *			- make adapter with  DrinkVolumes.COLUMN_VOLUME_OZ
			 *			- update to ML where appropriate (volume type 1)
			 *	- catch ontap and save selected volume's _ID in a variable
			 *
			 *	!! None of this actually matters - it's GUI only,
			 *	!! real stuff will happen by volume _ID
			 */

			volume_types_Spinner.setAdapter(adapter);
			
			volume_types_Spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				 public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {		 
						selected_volume_id = id;
						
						if(selected_volume_id == 999) { 	// 'custom' volume in DB
							custom_amount_EditText.setVisibility(View.VISIBLE);
							custom_amount_volume_types_Spinner.setVisibility(View.VISIBLE);
						} else {
							custom_amount_EditText.setVisibility(View.GONE);
							custom_amount_volume_types_Spinner.setVisibility(View.GONE);
						}
					  }
					 
					  @Override
					  public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
					  }
			});
		}
	}


}
