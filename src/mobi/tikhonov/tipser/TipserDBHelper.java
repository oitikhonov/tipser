package mobi.tikhonov.tipser;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import mobi.tikhonov.tipser.TipserDBContract.DrinkVolumes;
import mobi.tikhonov.tipser.TipserDBContract.Drinks;
import mobi.tikhonov.tipser.TipserDBContract.IntakeRecords;
import mobi.tikhonov.tipser.TipserDBContract.Limits;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TipserDBHelper extends SQLiteAssetHelper {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "tipser.db";

	private static TipserDBHelper sDBHelper = null;

	private TipserDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static TipserDBHelper getInstance(Context c) {
		if (sDBHelper == null) {
			sDBHelper = new TipserDBHelper(c.getApplicationContext());
		}
		return sDBHelper;
	}

}
