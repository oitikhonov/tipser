package mobi.tikhonov.tipser;

import mobi.tikhonov.tipser.R.id;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class TipserMainListViewFragment extends ListFragment {

    String[] items ={"Miller Genuine Draft", "Abita Andygator", "Asahi Super Dry",
	   "BACARDI BREEZER", "Boulevard Pale Ale", "Cascade Premium Light",
	   "Coopers Premium Lager", "Dogfish Head Palo Santo Marron", "Dogfish Head Shelter Pale Ale",
	   "Hornsby's Hard Crisp Apple Cider", "Monteith's Crushed Apple Cider", "vitae",
	   "Hornsby's Hard Crisp Apple Cider Hornsby's Hard Crisp Apple Cider Hornsby's Hard Crisp Apple Cider", "aliquet", "mollis",
	   "etiam", "vel", "erat",
	   "placerat", "ante",
	   "porttitor", "sodales",
	   "pellentesque", "augue",
	   "purus"};
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
	       super.onCreate(savedInstanceState);
	     //  getActivity().setTitle(R.string.app_name_lowercase); // it is ugly, because it's visible
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tipser_mainlistview_fragment_layout, parent, false);
		View header = inflater.inflate(R.layout.tipser_mainlistview_header_layout, null);
		
		ListView drinks_list = (ListView) v.findViewById(android.R.id.list);
		drinks_list.addHeaderView(header);
		
		Button add_drink_Button = (Button) v.findViewById(id.add_drink_Button);
		add_drink_Button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), AddDrink.class));
				
			}
		});
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.tipser_mainlistview_element_layout, R.id.main_list_element_drink_name_textView, items) {
			
			 @Override
			   public View getView(int position, View convertView, ViewGroup parent) {
				 View row=super.getView(position, convertView, parent);
			    
			          return row;
			   }
			
		};
		
		//setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items));
		setListAdapter(adapter);
		 
		return v;
	}
}
