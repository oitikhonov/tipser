package mobi.tikhonov.tipser;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;

public class AddDrink extends SingleFragmentActivity {

	protected Fragment createFragment() {

		return new AddDrinkFragment();
	}

}
