package mobi.tikhonov.tipser;

import android.provider.BaseColumns;

public final class TipserDBContract {
	
	public TipserDBContract() {
		// empty constructor
	}
	
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String FLOAT_TYPE = " REAL";
    private static final String COMMA_SEP = ", ";
	
	public static abstract class Limits implements BaseColumns {
		public static final String TABLE_NAME = "limits_"; // "_" + language code in ISO 639-1
		public static final String COLUMN_COUNTRY_NAME = "country_name";
		public static final String COLUMN_COUNTRY_LIMIT = "country_limit";
		public static final String COLUMN_COUNTRY_CODE = "country_code";
	}

	public static abstract class DrinkVolumes implements BaseColumns {
		public static final String TABLE_NAME = "drink_volumes_"; // "_" + language code in ISO 639-1
		public static final String COLUMN_VOLUME_NAME = "volume_name";
		public static final String COLUMN_VOLUME_OZ = "volume_usfloz";
		public static final String COLUMN_VOLUME_ML = "volume_ml";
		public static final String COLUMN_VOLUME_TYPE = "volume_type";
		public static final String COLUMN_DRINK_TYPE = "drink_type";
		public static final String COLUMN_CUSTOM_FLAG = "custom_flag";
		public static final String COLUMN_DELETED_FLAG = "deleted_flag";
	}
	
	public static abstract class Drinks implements BaseColumns {
		public static final String TABLE_NAME = "drinks_"; // "_" + language code in ISO 639-1
		public static final String COLUMN_DRINK_TYPE = "drink_type";
		public static final String COLUMN_DRINK_NAME = "drink_name";
		public static final String COLUMN_ALCO_PERCENTAGE = "alcohol_percentage";
		public static final String COLUMN_CUSTOM_FLAG = "custom_flag";
		public static final String COLUMN_DELETED_FLAG = "deleted_flag";
		public static final String COLUMN_USED_TIMES = "used_number";
	}
	
	public static abstract class IntakeRecords implements BaseColumns {
		public static final String TABLE_NAME = "intake_records"; // does not localise
		public static final String COLUMN_DRINK_ID = "drink_id";
		public static final String COLUMN_VOLUME_ID = "volume_id";
		public static final String COLUMN_CUSTOM_VOLUME = "custom_volume";
		public static final String COLUMN_CUSTOM_VOLUME_AMOUNT = "custom_volume_amount";
	}

}
