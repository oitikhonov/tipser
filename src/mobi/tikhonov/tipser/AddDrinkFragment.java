package mobi.tikhonov.tipser;

import mobi.tikhonov.tipser.R.id;
import mobi.tikhonov.tipser.TipserDBContract.Drinks;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class AddDrinkFragment extends Fragment {

	private TipserDBHelper db = null;
	private Spinner drink_types_Spinner = null; // check if all three are needed
	private EditText drink_search_editText = null;
	private ListView drinks_selection_listView = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		db = TipserDBHelper.getInstance(getActivity());
		// new LoadCursorTask().execute(); // to be implemented
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tipser_adddrink_fragment_layout,
				parent, false);

		drink_types_Spinner = (Spinner) v.findViewById(id.drink_type_Spinner);
		drinks_selection_listView = (ListView) v.findViewById(id.drinks_selection_listView);

		ArrayAdapter<String> drink_types_adapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_spinner_item,
				getResources().getStringArray(R.array.drink_types_array)) {

			/*
			 * @Override public View getView(int position, View convertView,
			 * ViewGroup parent) { View row=super.getView(position, convertView,
			 * parent);
			 * 
			 * return row; }
			 */

		};
	
		drink_types_Spinner.setAdapter(drink_types_adapter);
		
		drink_types_Spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener() {
		});

		drinks_selection_listView.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {	// id is _id
			    /*Toast.makeText(getActivity(),
			      "Position: " + position + " id: " + id, Toast.LENGTH_LONG)
			      .show();*/
			    
			    //new NewDrinkEditDialogFragment().show(getActivity().getSupportFragmentManager(), "editnewdrink_dialog");
			    NewDrinkEditDialogFragment.newInstance(id).show(getActivity().getSupportFragmentManager(), "editnewdrink_dialog");
			  }
			}); 
		
		return v;
	}
	
	public class CustomOnItemSelectedListener implements OnItemSelectedListener {
		 
		  public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
			Toast.makeText(parent.getContext(), 
				"pos: " + pos, //parent.getItemAtPosition(pos).toString(),
				Toast.LENGTH_SHORT).show();
			  new LoadDrinksCursorTask(pos + 1).execute();
		  }
		 
		  @Override
		  public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		  }
		 
	}

	
	private class LoadDrinksCursorTask extends AsyncTask<Void, Void, Void> {

		private Cursor local_cursor = null;
		private int drink_type = 0;
		
		LoadDrinksCursorTask(int drink_type) {
			this.drink_type = drink_type;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// don't forget to sort by COLUMN_USED_FLAG and then alphabetically,
			// case-insensitive
			local_cursor = db.getReadableDatabase().query(
					Drinks.TABLE_NAME + "en", // fix locale later
					new String[] { Drinks._ID, Drinks.COLUMN_DRINK_NAME,
							Drinks.COLUMN_ALCO_PERCENTAGE },
					Drinks.COLUMN_DRINK_TYPE + " = " + drink_type, null,
					null, null, null);
			local_cursor.getCount();

			return (null);
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onPostExecute(Void arg0) {

			SimpleCursorAdapter adapter;

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				adapter = new SimpleCursorAdapter(
						getActivity(),
						R.layout.tipser_addrink_element_layout,
						local_cursor,
						new String[] { 
							    Drinks.COLUMN_DRINK_NAME,
								Drinks.COLUMN_ALCO_PERCENTAGE },

						new int[] {
								R.id.adddrink_list_element_drink_name_textView,
								R.id.addrink_list_element_alcopercent_textView },
						0);
			} else {
				adapter = new SimpleCursorAdapter(
						getActivity(),
						R.layout.tipser_addrink_element_layout,
						local_cursor,
						new String[] { Drinks.COLUMN_DRINK_NAME,
								Drinks.COLUMN_ALCO_PERCENTAGE },

						new int[] {
								R.id.adddrink_list_element_drink_name_textView,
								R.id.addrink_list_element_alcopercent_textView });
			}

			drinks_selection_listView.setAdapter(adapter);
		}
	}

}
